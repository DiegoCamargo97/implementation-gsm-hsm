package com.example.implementation_gsm_hsm;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.google.firebase.messaging.FirebaseMessaging;

public class Token extends AppCompatActivity {

    private static final String TAG = Token.class.getName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_token);
        FirebaseMessaging.getInstance().getToken().addOnCompleteListener(task -> {
            if (!task.isSuccessful()) {
                Log.w(TAG, "Fetching FCM registration token failed", task.getException());
                return;
            }
            // Get new FCM registration token
            String token = task.getResult();

            // Log and toast
            String msg = "My firebase Token: " + token;
            Log.d(TAG, msg);
            Toast.makeText(Token.this, msg, Toast.LENGTH_SHORT).show();
        });
    }
}