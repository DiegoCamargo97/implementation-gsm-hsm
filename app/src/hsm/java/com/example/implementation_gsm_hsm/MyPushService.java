package com.example.implementation_gsm_hsm;

import android.util.Log;

import com.huawei.hms.push.HmsMessageService;

public class MyPushService extends HmsMessageService {


    private static final String TAG = "MyPushService";

    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);
        Log.d(TAG, "New token to Huawei Services" + s);
    }
}
